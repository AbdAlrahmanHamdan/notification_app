package com.example.subscriptionapp

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatDialogFragment
import kotlinx.android.synthetic.main.dialog_subscribed.view.*

class SubscribeDialog: AppCompatDialogFragment(), View.OnClickListener {
    private var dialogDelegate: DialogDelegate? = null
    private var vieww: View? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        val layoutInflater = activity!!.layoutInflater
        vieww = layoutInflater.inflate(R.layout.dialog_subscribed, null)
        builder.setView(vieww)
        vieww?.findViewById<Button>(R.id.cancelButton).also {
            it?.setOnClickListener(this)
        }
        vieww?.subscribeButton.also {
            it!!.setOnClickListener(this)
        }
        return builder.create()
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.cancelButton -> {
                dialogDelegate?.onCancel()
            }
            R.id.subscribeButton -> {
                val emailErrorMessage = verfyEmailAddress(vieww?.emailText?.text.toString())
                val usernameMessage = verfyUsername(vieww?.usernameText?.text.toString())
                if (emailErrorMessage != "Success") {
                    vieww?.emailError?.text = emailErrorMessage
                }
                else {
                    vieww?.emailError?.text = ""
                }

                if (usernameMessage != "Success"){
                    vieww?.usernameError?.text = usernameMessage
                }
                else {
                    vieww?.usernameError?.text = ""
                }

                if (usernameMessage == "Success" && emailErrorMessage == "Success") {
                    dialogDelegate?.onSubscribe(vieww?.emailText?.text.toString(), vieww?.usernameText?.text.toString())
                }

            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogDelegate = context as DialogDelegate
    }
}