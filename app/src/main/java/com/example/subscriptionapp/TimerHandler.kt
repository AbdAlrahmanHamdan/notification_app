package com.example.subscriptionapp

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent

class TimerHandler {
    private var context: Context? = null

    constructor(context:Context) {
        this.context = context
    }

    @SuppressLint("ShortAlarm")
    fun setTimer() {

        val intent = Intent(context, NotificationReceiver::class.java)
        intent.setAction("Notification_Application")

        val sender = PendingIntent.getBroadcast(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val timer = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val triggerEvery: Long = 5000
        timer.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerEvery, sender)
    }

    fun cancelTimer() {
        val intent = Intent("Notification_Application")
        val sender = PendingIntent.getBroadcast(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val timer = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        timer.cancel(sender)
    }
}