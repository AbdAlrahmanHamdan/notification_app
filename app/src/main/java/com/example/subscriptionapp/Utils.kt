package com.example.subscriptionapp

fun verfyEmailAddress(email: String?) : String {

    val emailRegex = "^[\\w.%+-]+@[\\w.-]+\\.[\\w]{2,6}$".toRegex()

    return if (email == null || email.isEmpty())
        "This field is required"

    else if (!emailRegex.matches(email))
        "Please enter a valid email"

    else
        "Success"
}

fun verfyUsername(username: String?): String {
    val usernameRegex = "^[a-zA-z0-9]{5,15}$".toRegex()

    return if (username == null || username.isEmpty())
        "This field is required"

    else if (!usernameRegex.matches(username))
        "Please enter a valid username"

    else
        "Success"
}