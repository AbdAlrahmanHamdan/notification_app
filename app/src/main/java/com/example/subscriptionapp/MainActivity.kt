package com.example.subscriptionapp

import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DialogDelegate {
    var subscribeDialog: SubscribeDialog? = null
    private val sharedPreferencesKey: String = "Subscription App"
    private var sharedPreference: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedPreference = getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE)
        if (loadData() == 2 || intent.getBooleanExtra("fromNotification", false)) {
            subscribeDialog = SubscribeDialog()
            subscribeDialog?.show(supportFragmentManager, "Subscribe Dialog")
        }
        else if (loadData() == 0)
            isSubscribed.text = "Not Subscribed"
        else
            isSubscribed.text = "Subscribed"

        val timerHandler = TimerHandler(this)
        timerHandler.cancelTimer()
        timerHandler.setTimer()
    }

    override fun onCancel() {
        subscribeDialog?.dismiss()
        sharedPreference?.edit()?.putInt("isSubscribed", 0)?.apply()
        isSubscribed.text = "Not Subscribed"
    }

    override fun onSubscribe(email: String?, username:String?) {
        subscribeDialog?.dismiss()
        sharedPreference?.edit()?.putInt("isSubscribed", 1)?.putString("Email", email)?.putString("Username", username)?.apply()
        isSubscribed.text = "Subscribed"
    }

    private fun loadData(): Int {
        return when {
            sharedPreference?.getInt("isSubscribed", 2) == 2 -> {2}
            sharedPreference?.getInt("isSubscribed", 2) == 1 -> {isSubscribed.text = "Not Subscribed"; return 1}
            else -> {isSubscribed.text = "Subscribed"; return 0}
        }
    }
}