package com.example.subscriptionapp

interface DialogDelegate {
    fun onCancel()
    fun onSubscribe(email: String?, username: String?)
}