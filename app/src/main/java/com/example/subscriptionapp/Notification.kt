package com.example.subscriptionapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class Notification{
    private val CHANNEL_ID = "Notification_Application"
    var context: Context? = null

    constructor(context: Context) {
        this.context = context
        createNotificationChannel()
    }

    private fun createNotificationChannel() {

        val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT)
        val notificationManager = context?.getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(notificationChannel)

        val mainIntent = Intent(context, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        mainIntent.putExtra("fromNotification", true)
        val mainPendingIntent = PendingIntent.getActivity(context!!, 100, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val buttonIntent = Intent(context, NotificationReceiver::class.java)
        val btPendingIntent = PendingIntent.getBroadcast(context, 100, buttonIntent, Intent.FILL_IN_DATA)

        val builder = NotificationCompat.Builder(context!!, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle("Notification Application")
            .setContentText("Subscribe for more")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setCategory(NotificationCompat.CATEGORY_REMINDER)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .addAction(R.drawable.gradient, "Subscribe", mainPendingIntent)
            .addAction(R.drawable.gradient, "Cancel", btPendingIntent)
            .setAutoCancel(true)

        NotificationManagerCompat.from(context!!).notify(1, builder.build())
    }
}