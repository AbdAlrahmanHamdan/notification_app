package com.example.subscriptionapp

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NotificationReceiver: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {


        if (p1!!.action.equals("Notification_Application"))
            Notification(p0!!)
        else {
            val manager: NotificationManager =
                p0!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.cancel(1)
            val timerHandler = TimerHandler(p0)
            timerHandler.cancelTimer()
            timerHandler.setTimer()
        }
    }
}